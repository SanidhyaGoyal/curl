import com.fasterxml.jackson.databind.ObjectMapper;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class RetrieveDeviceId {

    ObjectMapper Obj = new ObjectMapper();

    List<String> no = Arrays.asList("");

    public static void main(String arg[]) throws Exception {
        RetrieveDeviceId obj = new RetrieveDeviceId();
        obj.csvFilereaderOutput();
    }

    private void csvFilereader() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(
                getClass().getClassLoader().getResource("bb.csv").getFile()));
        HashSet<String> noSet = new HashSet<>(no);
        String line;
        while ((line = br.readLine()) != null) {
            String [] data = line.split(",");
            if(data.length == 4 && !noSet.contains(data[0]) && (!data[1].isEmpty())) {
                SpaRequest request = new SpaRequest(data[0], data[1], data[2], data[3]);
                sendPost(request);
            }
        }
    }

    private void csvFilereaderOutput() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(
                getClass().getClassLoader().getResource("bb.csv").getFile()));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println( "'" + line + "',");
        }
    }

    private void sendPost(SpaRequest req) throws Exception {

        String url = "https://api.staging-operation-link.com/spas";

        HttpsURLConnection httpClient = (HttpsURLConnection) new URL(url).openConnection();

        httpClient.setRequestMethod("POST");
        httpClient.setDoOutput(true);
        httpClient.setRequestProperty("Content-Type", "application/json; utf-8");
        httpClient.setRequestProperty("Accept", "application/json");
        httpClient.setRequestProperty("Authorization", "Bearer <PUT_BEARER_TOKEN_FROM_WEB_CONSOLE>");

        OutputStreamWriter osw = new OutputStreamWriter(httpClient.getOutputStream(), "UTF-8");
        osw.write(Obj.writeValueAsString(req));
        osw.flush();
        osw.close();

        int responseCode = httpClient.getResponseCode();
        System.out.println("Request for spa:" + Obj.writeValueAsString(req) + ". Response Code: " + responseCode);

        if(responseCode != 200){
            System.out.println("Request for spa:" + Obj.writeValueAsString(req) + ". Failed Reason: " + responseCode);
        } else {
            //Read the Response Body
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpClient.getInputStream()))) {
                String line;
                StringBuilder response = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    response.append(line);
                }
                //print Response
                System.out.println(response.toString());

            }
        }
    }
}