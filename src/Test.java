import javax.net.ssl.HttpsURLConnection;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.Scanner;

public class Test {
    public static void main(String args[]) throws IOException {

        String row = "";
        BufferedReader csvReader = new BufferedReader(new FileReader("C:/Users/admin/Desktop/curl/src/input.csv"));
        FileWriter fw = new FileWriter("C:/Users/admin/Desktop/curl/src/output.csv");
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");

            String deviceId = data[1];
            String accessToken = "6143877f856f6c3471d5b2a316bbe5bdd9384588";

            String url = "https://api.particle.io/v1/devices/" + deviceId + "/CMD?access_token=" + accessToken;

            //System.out.println(deviceId);
            //System.out.println(url);
            String cmdArr[] = {
                    "curl", "-L", "-X", "POST", url, "-H ",
                    "Content-Type: application/x-www-form-urlencoded",
                    "--data-urlencode", "arg=GETOPT"
            };


            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command(cmdArr);

            try {
                Process p = processBuilder.start();
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                    System.out.println("This is line -"+line);
                    System.out.println(line.substring(65,66));
                    builder.append(System.getProperty("line.separator"));
                    System.out.println("This is builder -"+builder);
                }
                String result = builder.toString();
                System.out.print(result);
                //System.out.print(result.substring(65,66));

                try {
                    fw.write(data[0] + "," + data[1]+","+result.substring(65,66)+"\n");
                    System.out.println("Success...");

                } catch (Exception e) {
                    System.out.println(e);
                }


            } catch (IOException e) {
                System.out.println("error");
                e.printStackTrace();
            }

        }
        fw.close();
        csvReader.close();
    }
}
